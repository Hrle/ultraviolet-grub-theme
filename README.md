# `GRUB` ultraviolet theme

Ultraviolet theme for `GRUB`.

I don't know where I got this theme from, but I modified it and put it here.
If anyone has a problem with me distributing the theme here,
please contact me through this repository.

## Install

Copy [ultraviolet](ultraviolet) into `/usr/share/grub/themes` and write this:

```conf
GRUB_THEME="/usr/share/grub/themes/ultraviolet/theme.txt"
```

inside `/etc/default/grub`.
